import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BackendService } from '../backend.service';
import { NotificationsService } from '../notifications.service';
import { UserService } from '../user.service';
import { WebSocketService } from '../web-socket.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.sass']
})
export class ProfilComponent implements OnInit {

  constructor(private backend: BackendService, private notifications: NotificationsService, private route: ActivatedRoute, private webSocket : WebSocketService, private userService: UserService) {
    this.userMap = new Map<any, any>();
  }

  ngOnInit(): void {
    this.LoadProfile();
  }

  LoadProfile(): void {
    // We retrieve the profile data
    this.route.params.subscribe(params => {
      this.backend.Profile(params.username).subscribe(result => {
        this.data = result.profile;
        // We retrieve the user's history
        this.backend.History(this.data.identifiant).subscribe(result => {
          this.history = result.records.sort((a, b) => {
            return new Date(b.date_jeu).getTime() - new Date(a.date_jeu).getTime();
          });
        }, error => {
          this.notifications.DisplayNotification("Une erreur est survenue lors de la récupération de votre historique de jeu. (Log disponible dans la console)", "danger");
        })
      });

      this.backend.HistoryChallenges(params.username).subscribe(result => {
        let map = new Map<any, any>();
        // Fetching avatars
        result.records.forEach(element => {
          this.backend.ProfileById(element.id_user_gagnant).subscribe(data => {
            map[element.id_user_gagnant] = data.profile;
            ++this.challengeCount;
            if(this.challengeCount == result.records.length * 2) {
              this.userMap = map;
              this.challenges = result.records.sort((a, b) => {
                return new Date(b.date_defi).getTime() - new Date(a.date_defi).getTime();
              });
            }
          });
          this.backend.ProfileById(element.id_user_perdant).subscribe(data => {
            map[element.id_user_perdant] = data.profile;
            ++this.challengeCount;
            if(this.challengeCount == result.records.length * 2) {
              this.userMap = map;
              this.challenges = result.records.sort((a, b) => {
                return new Date(b.date_defi).getTime() - new Date(a.date_defi).getTime();
              });
            }    
          });
        });
      });
    });
  }

  ChangeAvatar(): void {
    this.backend.ChangeAvatar(this.avatar.value).subscribe((result) => {
      this.notifications.DisplayNotification("Avatar modifié avec succès", "success");
      this.LoadProfile();
    })
    this.avatar.setValue("");
  }

  ChangeMood(): void {
    this.backend.ChangeMood(this.mood.value).subscribe((result) => {
      this.notifications.DisplayNotification("Humeur modifiée avec succès", "success");
      this.LoadProfile();
    })
    this.mood.setValue("");
  }

  avatar = new FormControl('');
  mood = new FormControl('');

  data = undefined;
  challenges;
  history;
  challengeCount = 0;
  userMap : Map<any, any>;
}
