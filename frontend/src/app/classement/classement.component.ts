import { createTokenForExternalReference } from '@angular/compiler/src/identifiers';
import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-classement',
  templateUrl: './classement.component.html',
  styleUrls: ['./classement.component.sass']
})
export class ClassementComponent implements OnInit {

  constructor(private backend: BackendService) { }

  ngOnInit(): void {
    // On init, we retrieve the rankings and sort them from best to worst
    this.backend.Ranking().subscribe(result => {
      this.ranking = result.records;
      this.ranking.sort((a, b) => {
        return b.total - a.total;
      });
    }, error => {
      console.log(error);
    });
  }

  ranking;

}
