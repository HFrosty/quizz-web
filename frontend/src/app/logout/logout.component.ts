import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from '../backend.service';
import { NotificationsService } from '../notifications.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.sass']
})
export class LogoutComponent implements OnInit {

  constructor(private backend : BackendService, private userService : UserService, private notifications : NotificationsService, private router: Router) {

  }

  ngOnInit(): void {
    this.backend.LogOut().subscribe((result : any) => {
      // If logout is validated by server, inform the user via notification and tell the user service to proceed to the disconnection procedure
      this.pending = false;
      this.notifications.DisplayNotification("Déconnexion effectuée avec succès", "success");
      this.userService.Disconnect();
      this.router.navigateByUrl("login");
    }, (error : any) => {
      // If logout is unvalid (eg: logout while we have no session variable with backend), we inform user of error
      console.log(error);
      this.pending = false;
      this.notifications.DisplayNotification("Erreur lors de la déconnexion", "danger");
    });
  }

  // Variable used to display (or not) spinner
  pending : boolean = true;
}
