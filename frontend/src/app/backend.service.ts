import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

// Service used to communicate with backend
export class BackendService {

  constructor(private httpClient: HttpClient) { }

  public LogIn(username : string, password : string) {
    return this.httpClient.post<any>(BackendService.LOCATION + "login", {username: username, password: password}, this.requestOptions);
  }

  public LogOut() {
    return this.httpClient.post<any>(BackendService.LOCATION + "logout", {}, this.requestOptions);
  }

  public QuizzList() {
    return this.httpClient.get<any>(BackendService.LOCATION + "quizz-list", this.requestOptions);
  }

  public Quizz(theme: string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "quizz?theme=" + theme, this.requestOptions);
  }

  public GameEnd(difficulty: string, correctCount: number, time: number, score: number) {
    return this.httpClient.post<any>(BackendService.LOCATION + "game-end", {difficulty: difficulty, correctCount: correctCount, time: time, score: score}, this.requestOptions);
  }

  public History(username: string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "history/" + username, this.requestOptions);
  }

  public HistoryChallenges(username: string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "history-challenges/" + username, this.requestOptions);
  }

  public Ranking() {
    return this.httpClient.get<any>(BackendService.LOCATION + "ranking", this.requestOptions);
  }

  public Profile(username : string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "profile/" + username, this.requestOptions);
  }

  public ProfileById(id : string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "profile-by-id/" + id, this.requestOptions);
  }

  public Challenges(username: string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "pending-challenges", this.requestOptions);
  }

  public DenyChallenge(challenge: any) {
    return this.httpClient.post<any>(BackendService.LOCATION + "deny-challenge", {challengeId: challenge._id},this.requestOptions);
  }

  public ChallengeEnd(challengeId: string, sender: string, senderScore: number, receiverScore: number) {
    return this.httpClient.post<any>(BackendService.LOCATION + "challenge-end", {challengeId: challengeId, sender: sender, senderScore: senderScore, receiverScore: receiverScore}, this.requestOptions);
  }

  public TopTen() {
    return this.httpClient.get<any>(BackendService.LOCATION + "top-ten", this.requestOptions);
  }

  public ChangeAvatar(avatar: string) {
    return this.httpClient.patch<any>(BackendService.LOCATION + "change-avatar", {avatar: avatar}, this.requestOptions);
  }

  public ChangeMood(mood: string) {
    return this.httpClient.patch<any>(BackendService.LOCATION + "change-mood", {mood: mood}, this.requestOptions);
  }

  public NotificationFetch(username: string) {
    return this.httpClient.get<any>(BackendService.LOCATION + "notification-fetch/" + username, this.requestOptions);
  }

  private requestOptions = {
    withCredentials: true,
  };
  private static LOCATION = "http://pedago.univ-avignon.fr:3031/";
}
