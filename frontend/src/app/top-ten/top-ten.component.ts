import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-top-ten',
  templateUrl: './top-ten.component.html',
  styleUrls: ['./top-ten.component.sass']
})
export class TopTenComponent implements OnInit {

  constructor(private backend: BackendService) { }

  ngOnInit(): void {
    this.backend.TopTen().subscribe(data => {
      this.top = data.records;
    })
  }

  top = null;
}
