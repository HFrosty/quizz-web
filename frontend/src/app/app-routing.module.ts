import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassementComponent } from './classement/classement.component';
import { DefisComponent } from './defis/defis.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfilComponent } from './profil/profil.component';
import { QuizzComponent } from './quizz/quizz.component';
import { TopTenComponent } from './top-ten/top-ten.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'profil/:username', component: ProfilComponent},
  {path: 'quizz', component: QuizzComponent},
  {path: 'classement', component: ClassementComponent},
  {path: 'defis', component: DefisComponent},
  {path: 'top-ten', component: TopTenComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
