import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { BackendService } from './backend.service';

// User service is used to determine if the user is logged in, to log the user, to log out...

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private constructor(private storage: StorageMap, private backend: BackendService) {
    // If we have user data stored, it means we are logged in (because the data is deleted on logout) and we retrieve the data
    // storing in local storage allows for user to stay connected after closing browser or refreshing the page
    this.storage.get("USERNAME").subscribe((result : any) => {
      if(result != undefined) {
        UserService.username = result.username;
      }
    });
  }

  public LogIn(userData: any) {
    UserService.username = userData.identifiant;
    // Store login info so it can be retrieved at next login
    this.storage.set("LAST_LOGIN", {username: userData.identifiant, date: new Date()}).subscribe(() => {});
    // Store userdata so it can be retrieved when we create another UserService
    this.storage.set("USERNAME", {username: userData.identifiant}).subscribe(() => {});
    this.backend.NotificationFetch(userData.identifiant).subscribe(data => {

    });
  }

  public Disconnect() {
    // Delete user data
    this.storage.delete("USERNAME").subscribe(() => {
      UserService.username = null;
    });
  }

  public IsLoggedIn() : boolean {
    // If we have stored user data, it means we are logged in
    return UserService.username != null;
  }

  public GetUsername() : any {
    return UserService.username;
  }

  private static username : any = null;
}
