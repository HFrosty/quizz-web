import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { NotificationsService } from '../notifications.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-defis',
  templateUrl: './defis.component.html',
  styleUrls: ['./defis.component.sass']
})
export class DefisComponent implements OnInit {

  constructor(private backend: BackendService, private user: UserService, private notifications: NotificationsService) {
    this.challenges = new Array<any>();
  }

  ngOnInit(): void {
    this.state = "NORMAL";
    this.avatarMap = new Map<string, string>();
    // We fetch the logged user's challenges
    this.backend.Challenges(this.user.GetUsername()).subscribe(data => {
      this.challenges = data.asReceiver;
      data.asSender.forEach(element => {
        this.challenges.push(element);
      });

      // Fetching avatars
      this.challenges.forEach(element => {
        if(this.avatarMap.has(element.sender)) {
          return;
        }
        this.avatarMap[element.sender] = "";
        this.backend.Profile(element.sender).subscribe(data => {
          this.avatarMap[element.sender] = data.profile.avatar;
        });

        if(this.avatarMap.has(element.receiver)) {
          return;
        }
        this.avatarMap[element.receiver] = "";
        this.backend.Profile(element.receiver).subscribe(data => {
          this.avatarMap[element.receiver] = data.profile.avatar;
        })
      });
    });
  }

  // Used by the "REFUSER" button to deny a challenge
  RemoveChallenge(challenge: any) : void {
    this.challenges.splice(this.challenges.indexOf(challenge), 1);
    this.backend.DenyChallenge(challenge).subscribe(result => {
      this.notifications.DisplayNotification("Défi refusé avec succès.", "success");
    }, error => {
      this.notifications.DisplayNotification("Une erreur est survenue lors de l'annulation du défi.", "danger");
    });
  }

  // Used by the "ACCEPTER" button to deny a challenge
  // When accepting a challenge, we display the quizz component by injecting it with the challenge data for it to retrieve the difficulty and theme of the challenge
  AcceptChallenge(challenge: any) : void {
    this.state = "QUIZZ";
    this.selectedChallenge = challenge;
  }

  state : string;
  challenges;
  avatarMap : Map<string, string>;
  selectedChallenge;
}
