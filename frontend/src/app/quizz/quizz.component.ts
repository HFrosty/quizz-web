import { noUndefined } from '@angular/compiler/src/util';
import { Component, Input, OnInit } from '@angular/core';   
import { BackendService } from '../backend.service';
import { NotificationsService } from '../notifications.service';
import { UserService } from '../user.service';
import { WebSocketService } from '../web-socket.service';

@Component({
  selector: 'app-quizz',
  templateUrl: './quizz.component.html',
  styleUrls: ['./quizz.component.sass']
})
export class QuizzComponent implements OnInit {

  constructor(private backend : BackendService, private notifications : NotificationsService, private webSocket : WebSocketService, private userService : UserService) {
  }

  ngOnInit(): void {
    // On init, we say that we are loading (to display the spinner), and we load the quizz list
    if(!this.multiplayer) { // If the user is not responding to a challenge
      this.state = "LOADING";
      this.LoadQuizzList();
    } else { // Routine if the user is responding to a challenge, we set the difficulty and theme accordingly and load the data
      this.difficulty = this.challenge.difficulty;
      this.theme = this.challenge.theme;
      this.LoadThemeQuestions(() => {
        this.backend.Profile(this.challenge.sender).subscribe(data => {
          this.otherAvatar = data.profile.avatar;
        });
        this.backend.Profile(this.userService.GetUsername()).subscribe(data => {
          this.avatar = data.profile.avatar;
        });
        this.QuizzPreparation();
        this.state = "QUIZZ";
      });
    }
    this.currentGame = 0;
  }

  LoadQuizzList() : void {
    // We store the quizz list and we update the current state
    this.backend.QuizzList().subscribe(result => {
      this.quizzlist = result;
      this.state = "LIST";
    }, (error) => {
      this.notifications.DisplayNotification("ERROR", error);
    })
  }

  LoadThemeQuestions(endCallback) : void {
    this.backend.Quizz(this.theme).subscribe(result => {
      this.questionlist = new Array<any>();
      // Take 10 random quizz questions
      // Add each possible index to a set
      let nonUsed = new Set<number>();
      for(let i = 0; i < result.length; ++i) {
        nonUsed.add(i);
      }
      // Repeat 10 times: take a random index from the set and remove it, store the associated question to the list of questions that will be used
      for(let i = 0; i < 10; ++i) {
        let index = Math.floor(Math.random() * nonUsed.size);
        let toArray = Array.from(nonUsed);
        this.questionlist.push(result[toArray[index]])
        nonUsed.delete(toArray[index]);
        endCallback();
      }
    }, error => {
      this.notifications.DisplayNotification(error, "danger");
    })
  };

  // When the user selects a quizz, we retrieve the data conerning it from the backend
  OnQuizzSelection(theme: string) : void {
    this.theme = theme;
    this.state = "LOADING";
    this.LoadThemeQuestions(() => {
      // We go into the difficulty selection state
      this.state = "DIFFICULTY";
    });
  }

  // When we select the difficulty, we generate the propositions for the first question
  // Hard = 4 answers, Medium = 3 answers, Easy = 3 answers
  OnDifficultySelection(difficulty: string) : void {
    this.difficulty = difficulty;
    this.state = "QUIZZ";
    this.QuizzPreparation();
  }

  // We prepare the variables
  QuizzPreparation() : void {
    this.startTime = Math.round(new Date().getTime() / 1000);
    this.current = 0;
    this.correctCount = 0;
    this.score = 0;
    this.GeneratePropositions();
    this.TimerRestart();
  }

  // When the user goes to the next question, we check if his answer was correct and update his score accordingly
  // If we were at the last question, we go to the end state and store the game that he played in the database
  // If we were not, we restart the timer (at 10) and generate propositions for the new question
  NextQuestion(answer: string) : void {
    if(this.questionlist[this.current].réponse == answer) {
      let multiplier = this.difficulty == "HARD" ? 2.0 : this.difficulty == "MEDIUM" ? 1.5 : 1.0;
      this.score += 100 * this.timer * multiplier;
      ++this.correctCount;
    }
    if(this.current == this.questionlist.length - 1) {
      this.duration = Math.round(new Date().getTime() / 1000) - this.startTime;
      this.OnQuizzEnd();
    } else {
      this.current++;
      this.TimerRestart();
      this.GeneratePropositions();
    }
  }

  // The routine for the countdown
  // We wait 1 second to decrement the timer, if it reaches zero, we skip to the next question, if not, we redo the routine and decrement the timer
  // We have to store the game in which the routine was started, to avoid decrementing the timer of the next game
  // We also have to store the question in which the routine was started, in order to avoid decrementing the timer of the next question
  TimerRoutine() : void {
    let current = this.current;
    let currentGame = this.currentGame;
    setTimeout(() => {
      if(this.current == current && this.currentGame == currentGame) {
        if(this.timer == 0) {
          this.NextQuestion("none");
        } else {
          this.timer--;
          this.TimerRoutine();
        }
      }
    }, 1000);
  }

  // Called when we need to restart the timer -> just after selecting a difficulty, or just after a questoin
  TimerRestart() : void {
    this.timer = 10;
    this.TimerRoutine();
  }

  // We randomize the proposition for the current question
  // We always keep the correct answer and select more or less of the others depending on the difficulty setting
  GeneratePropositions() : void {
    let nonUsed = new Set<number>();
    this.propositions = new Array<string>();
    // Add correct answer
    let current = this.questionlist[this.current];
    let correct = current.réponse;
    for(let i = 0; i < 4; ++i) {
      nonUsed.add(i);
      if(correct == current.propositions[i]){
        nonUsed.delete(i);
        this.propositions.push(current.propositions[i]);
      }
    }
    // Randomly add non used answers
    let max = this.difficulty == "HARD" ? 3 : this.difficulty == "MEDIUM" ? 2 : 1;
    for(let i = 0; i < max; ++i) {
      let index = Math.floor(Math.random() * nonUsed.size);
      let toArray = Array.from(nonUsed);
      this.propositions.push(current.propositions[toArray[index]]);
      nonUsed.delete(toArray[index]);
    }
    // Randomize the order
    for(let i = 0; i < this.propositions.length - 1; ++i) {
      let aIndex = Math.floor(Math.random() * this.propositions.length);
      let bIndex = Math.floor(Math.random() * this.propositions.length);
      let tmp = this.propositions[aIndex];
      this.propositions[aIndex] = this.propositions[bIndex];
      this.propositions[bIndex] = tmp;
    }
  }

  // Method used when we switch to challenge state
  // In this state, the list of other users is fetched and then displayed on the screen
  Challenge() : void {
    this.state = "CHALLENGE";
    this.backend.Ranking().subscribe(data => {
      this.ranking = data.records;
    });
  }

  // Method used when the user clicks on another user, notifying the backend that the user is willing to challenge the other
  ChallengeUser(username: string) : void {
    let data = {
      sender: this.userService.GetUsername(),
      score: this.score,
      receiver: username,
      theme: this.theme,
      difficulty: this.difficulty
    };
    this.webSocket.Send("challenge", data);
    this.Replay();
  }

  // Used to display the list of available themes
  Replay() : void {
    this.state = "LIST";
  }

  // What happens when a quizz end
  // We notify the backend of the play and if the user is answering to a challenge, we notify the backend that a challenge has ended by providing it the results
  OnQuizzEnd() : void {
    ++this.currentGame;
    this.backend.GameEnd(this.difficulty, this.correctCount, this.duration, this.score).subscribe(result => {
      this.notifications.DisplayNotification("Partie enregistrée avec succès!", "success")
    }, error => {
      this.notifications.DisplayNotification(error, "danger");
    })
    this.state = "END";
    if(!this.multiplayer) {
      return;
    }

    this.backend.ChallengeEnd(this.challenge._id, this.challenge.sender, this.challenge.score, this.score).subscribe(result => {
      this.notifications.DisplayNotification("Défi terminé", "success");
    });
  }

  @Input() multiplayer : boolean = false;
  @Input() challenge: any;
  otherAvatar : string;
  avatar : string;

  currentGame : number;
  state : string = "";

  quizzlist : Array<any>;
  theme : string;
  difficulty : string;

  propositions : Array<any>;
  questionlist : Array<any>;

  startTime : number;
  duration : number;
  timer : number;
  current : number;
  score : number;
  correctCount : number;

  ranking : any;
}
