import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageMap } from '@ngx-pwa/local-storage';
import { BackendService } from '../backend.service';
import { NotificationsService } from '../notifications.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  public loginForm;

  constructor(private formBuilder: FormBuilder, private backend : BackendService, private userService : UserService, private notifications : NotificationsService, private storage: StorageMap, private router: Router) {
    this.loginForm = this.formBuilder.group({
      username: '',
      password: ''
    })
  }

  ngOnInit(): void {
  }

  OnSubmit(loginData): void {
    this.pending = true;
    // We send a login request to the backend
    this.backend.LogIn(loginData.username, loginData.password).subscribe(result => {
      this.pending = false;
      // When we receive the response, we check if we have a last login data stored in the browser, if so, we modify the display message accordingly
      this.storage.get("LAST_LOGIN").subscribe((lastLoginData : any) => {
        let relativeToLastConnection = "";
        if(lastLoginData != undefined) {
          relativeToLastConnection = ", last connection was " + lastLoginData.date + " with username: " + lastLoginData.username;
        }
        // We send the response user data to the user service
        this.userService.LogIn(result.user);
        // We display the notification
        this.notifications.DisplayNotification("Successfully connected with username " + loginData.username + relativeToLastConnection, "success");
        // We move to the profile page
        this.router.navigateByUrl("profil/" + result.user.identifiant);
      });
    }, error => {
      console.log(error);
      // If login is invalid (bad credentials, or trying to log in while we already have a session variable with backend), we display an error message
      this.pending = true;
      this.notifications.DisplayNotification("Unable to connect, incorrect login/password combination", "danger");
    });
  }

  pending : boolean = false;

}
