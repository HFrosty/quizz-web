import { Component, OnInit } from '@angular/core';
import { NotificationsService } from './notifications.service';
import { UserService } from './user.service';
import { WebSocketService } from './web-socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'The CERI Game';

  constructor(private webSocket: WebSocketService, private notifications: NotificationsService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.webSocket.Listen('atLogin').subscribe(data => {
      if(!this.userService.IsLoggedIn() || this.userService.GetUsername() == data.username) {
        return;
      }
      this.notifications.DisplayNotification(data.username + " vient de se connecter", "info");
    });

    this.webSocket.Listen('atLogout').subscribe(data => {
      if(!this.userService.IsLoggedIn() || this.userService.GetUsername() == data.username) {
        return;
      }
      this.notifications.DisplayNotification(data.username + " vient de se déconnecter", "info");
    });

    this.webSocket.Listen('newChallenge').subscribe(data => {
      console.log(!this.userService.IsLoggedIn() || this.userService.GetUsername() != data.receiver);
      if(!this.userService.IsLoggedIn() || this.userService.GetUsername() != data.receiver) {
        return;
      }
      this.notifications.DisplayNotification(data.sender + " vous a défié!", "info");
    });
  }
}
