import { parseHostBindings } from '@angular/compiler';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

// The NotifcationService uses a list of pending notifications and displays them in order with 2s between each one
export class NotificationsService {

  private constructor() {
  }

  private DisplayNext() : void {
    if(NotificationsService.pending.length == 0 || NotificationsService.displaying == true) {
      return;
    }
    NotificationsService.displaying = true;

    let notification = NotificationsService.pending.shift();

    let caption = document.getElementById("notifications");
    // We give corresponding type to notification element
    let colors = ["primary", "secondary", "success", "danger", "warning", "info", "light", "dark"];
    colors.forEach(element => {
      caption.classList.remove("alert-" + element);
    });
    caption.classList.add("alert-" + notification.type);
    caption.classList.remove("d-none");

    caption.innerText = notification.text;
    // We hide notification two seconds after it spawned
    setTimeout(() => {
      caption.classList.add("d-none");
      NotificationsService.displaying = false;
      this.DisplayNext();
    }, 1900);
  }

  public DisplayNotification(text: string, type: string) : void {
    NotificationsService.pending.push({text: text, type: type});
    this.DisplayNext();
  }

  private static pending : Array<any> = new Array<any>();
  private static displaying : boolean = false;
}
