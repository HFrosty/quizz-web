import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  constructor() {
    this.socket = io('http://pedago.univ-avignon.fr:3031');
  }

  Listen(event : string) : Observable<any> {
    return new Observable((subscribe) => {
      this.socket.on(event, (data) => {
        subscribe.next(data);
      })
    })
  }

  Send(event : string, data : any) {
    this.socket.emit(event, data);
  }

  socket;
}
