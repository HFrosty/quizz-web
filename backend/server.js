const sha1 = require('sha1');
var express = require('express');
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
const mongodb = require('mongodb');
const MongoClient = require('mongodb').MongoClient;
const cors = require('cors');
const bodyParser = require('body-parser');
const pgsql = require('./PGSQL');
const { exit } = require('process');
const { ETIME } = require('constants');
const { request, response } = require('express');

const app = express();
const PORT = 3031;
const parser = bodyParser.json();

const DB_URL = "mongodb://localhost:27017";
//const CLIENT_URL = "http://127.0.0.1:4200";
const CLIENT_URL = "http://pedago.univ-avignon.fr:3032";
var MONGO_DB = null;

var store = new MongoDBStore({
	uri: DB_URL,
	collection: 'thesessionsofthequizz'
});


app.use(cors({ origin: CLIENT_URL, credentials: true }));

app.use(session({
	secret: 'PatrickStar',
	cookie: {
		maxAge: 1000 * 60 * 60 * 24 * 7, // 1 week
		secure: false
	},
	store: store,
	resave: true,
	saveUninitialized: true
}));

app.get("/", (request, response) => {
	response.send({ test: "test" });
});

// Method used when the user request for a login
// When this method is used, we set in the database that the player is connected
// Input : username and password
// Output : user data and session last session username
app.post("/login", parser, (request, response) => {
	/*if (request.session.userId != undefined) {
		response.statusCode = 403;
		response.send({ status: "BAD", message: "ALREADY LOGGED IN" });
		return;
	}*/

	let query = {
		text: 'SELECT * FROM fredouil.users WHERE identifiant=$1 AND motpasse=$2',
		values: [request.body.username, sha1(request.body.password)],
	};

	pgsql.ExecuteQuery(query).then(result => {
		if (result.message.rows[0] == undefined) {
			response.statusCode = 401;
			response.send({ status: "ERROR", message: "NO USER" });
			return;
		}

		let statusUpdateQuery = {
			text: 'UPDATE fredouil.users SET statut_connexion=$1 WHERE id=$2',
			values: [1, result.message.rows[0].id]
		};

		pgsql.ExecuteQuery(statusUpdateQuery).then(statusUpdateResult => {
			result.message.rows[0].statut_connexion = 1;
			request.session.userId = result.message.rows[0].id;
			request.session.username = result.message.rows[0].identifiant;
			response.statusCode = 200;
			response.send({ status: "GOOD", user: result.message.rows[0], last: request.session.attemptedUsername });
			io.emit('atLogin', {username: result.message.rows[0].identifiant} );
		}, error => {
			response.statusCode = 500;
			response.send({ status: "ERROR", message: error });
		});
	}, error => {
		response.statusCode = 500;
		response.send({ status: "ERROR", message: error });
	});
});

// Method used when login out
// When this method is used, we set in the database that the player is logged out
// Also destroys session
app.post("/logout", parser, (request, response) => {
	if (request.session.userId == undefined) {
		response.statusCode = 403;
		response.send({ status: "BAD", message: "NOT LOGGED IN" });
		return;
	}

	let query = {
		text: 'UPDATE fredouil.users SET statut_connexion=$1 WHERE id=$2',
		values: [0, request.session.userId]
	};

	pgsql.ExecuteQuery(query).then(result => {
		io.emit('atLogout', {username: request.session.username});
		request.session.destroy();
		response.statusCode = 200;
		response.send({ status: "GOOD", message: "DISCONNECTION SUCCESSFUL" });
	}, error => {
		response.statusCode = 500;
		response.send({status: "BAD", message: error});
	});
});

app.get("/profile/:username", parser, (request, response) => {
	let query = {
		text: 'SELECT fredouil.users.id, identifiant, nom, prenom, date_naissance, avatar, humeur, statut_connexion, COUNT(id_user_gagnant) FROM fredouil.hist_defi INNER JOIN fredouil.users ON fredouil.users.id = fredouil.hist_defi.id_user_gagnant WHERE fredouil.users.identifiant=$1 GROUP BY fredouil.users.id, identifiant, nom, prenom, date_naissance, avatar, humeur, statut_connexion;',
		values: [request.params.username]
	};
	pgsql.ExecuteQuery(query).then(results => {
		response.statusCode = 200;
		response.send({status: "GOOD", profile: results.message.rows[0]});
	}, error => {
		response.statusCode = 500;
		response.send({status: "BAD", message: error});
	})
});

app.get("/notification-fetch/:username", parser, (request, response) => {
	// When someone logs in, we check if a challenge notification is pending for him
	let filter = {
		receiver: request.params.username
	}
	MONGO_DB.collection("pending-notifications").find(filter).toArray((error, result) => {
		if(error) {
			response.statusCode = 500;
			response.send({ status: "ERROR", message: error });
			return;
		}
		result.forEach((element) => {
			io.emit('newChallenge', {
				sender: element.sender,
				receiver: element.receiver
			});
			MONGO_DB.collection("pending-notifications").deleteOne({_id: new mongodb.ObjectID(element._id)}, (error, result) => {
				if(error) {
					console.log(error);
				}
			});
		})
		response.statusCode = 200;
		response.send({status: "GOOD"});
	})
});

// Method used to retrieve the quizz on the MongoDB database
// Output : quizzes themes and id
app.get("/quizz-list", parser, (request, response) => {
	let fields = {
		thème: 1,
	}

	MONGO_DB.collection("quizz").find().project(fields).toArray((error, records) => {
		if (error != null) {
			response.statusCode = 500;
			response.send({ message: "Unable to execute query" });
		}
		records.forEach(element => {
			element.theme = element.thème;
			delete element.thème;
		});
		response.statusCode = 200;
		response.send(records);
	});
});

// Method used to retrieve a specific quizz data
// Input : Quizz theme
// Output : all quizz data
app.get("/quizz", parser, (request, response) => {
	let filter = {
		thème: request.query.theme
	};

	MONGO_DB.collection("quizz").find(filter).toArray((error, records) => {
		if (error != null) {
			response.statusCode = 500;
			response.send({ message: "Unable to execute query" });
		}
		if (records.length == 0) {
			response.statusCode = 404;
			response.send({ message: "No quizz found" });
		} else {
			response.statusCode = 200;
			response.send(records[0].quizz);
		}
	});
});

// Method used when a game has ended to store it in the history database
// Input : difficulty, number of correct answers, quizz duration, quizz score
app.post("/game-end", parser, (request, response) => {
	let gameLevel = request.body.difficulty == "HARD" ? 3 : request.body.difficulty == "MEDIUM" ? 2 : 1;
	let query = {
		text: 'INSERT INTO fredouil.historique(id_user, date_jeu, niveau_jeu, nb_reponses_corr, temps, score) VALUES($1, $2, $3, $4, $5, $6)',
		values: [request.session.userId, new Date(), gameLevel, request.body.correctCount, request.body.time, request.body.score],
	};

	pgsql.ExecuteQuery(query).then(result => {
		if (result.status == "GOOD") {
			response.statusCode = 200;
			response.send({ message: "GOOD" });
		} else {
			response.statusCode = 500;
			response.send({ message: "ERROR" });
		}
	});
});

// Method used when a player has ended a challenge
// Input: usernames and scores of both player
// using the scores, we determine who is the winner and who is the loser and add an entry in the database
app.post("/challenge-end", parser, (request, response) => {
	let query = {
		text: "SELECT id FROM fredouil.users WHERE identifiant=$1",
		values: [request.body.sender]
	};
	pgsql.ExecuteQuery(query).then(results => {
		let senderId = results.message.rows[0].id;
		query = {
			text: "SELECT id FROM fredouil.users WHERE identifiant=$1",
			values: [request.session.username]
		};
		pgsql.ExecuteQuery(query).then(results => {
			let receiverId = results.message.rows[0].id;
			let winner = request.body.senderScore > request.body.receiverScore ? senderId : receiverId;
			let loser = request.body.senderScore > request.body.receiverScore ? receiverId : senderId;
			query = {
				text: "INSERT INTO fredouil.hist_defi(id_user_gagnant, id_user_perdant, date_defi) VALUES($1, $2, $3)",
				values: [winner, loser, new Date()]
			};
			pgsql.ExecuteQuery(query).then(results => {
				MONGO_DB.collection("defis").deleteOne({_id: new mongodb.ObjectID(request.body.challengeId)}, (error, result) => {
					if(error != null || result.result.n != 1) {
						response.statusCode = 500;
						response.send({ message: "Unable to execute query" });
						return;
					}
					response.statusCode = 200;
					response.send({message: "GOOD"});
				});
			})
		})
	})
});

// Method used to fetch the finished challenges of a user by using his username
app.get("/history-challenges/:username", parser, (request, response) => {
	let query = {
		text: "SELECT id FROM fredouil.users WHERE identifiant=$1",
		values: [request.params.username]
	};
	pgsql.ExecuteQuery(query).then(results => {
		let id = results.message.rows[0].id;
		query = {
			text: "SELECT id_user_gagnant, id_user_perdant, date_defi FROM fredouil.hist_defi WHERE id_user_gagnant=$1 OR id_user_perdant=$1",
			values: [id]
		};
		pgsql.ExecuteQuery(query).then(results => {
			response.statusCode = 200;
			response.send({message: "GOOD", records: results.message.rows});
		});
	});
});

// Method used to retrieve all sort of data by using a user's id
app.get("/profile-by-id/:id", parser, (request, response) => {
	let query = {
		text: 'SELECT fredouil.users.id, identifiant, nom, prenom, date_naissance, avatar, humeur, statut_connexion, COUNT(id_user_gagnant) FROM fredouil.hist_defi INNER JOIN fredouil.users ON fredouil.users.id = fredouil.hist_defi.id_user_gagnant WHERE fredouil.users.id=$1 GROUP BY fredouil.users.id, identifiant, nom, prenom, date_naissance, avatar, humeur, statut_connexion;',
		values: [request.params.id]
	};
	pgsql.ExecuteQuery(query).then(results => {
		response.statusCode = 200;
		response.send({status: "GOOD", profile: results.message.rows[0]});
	}, error => {
		response.statusCode = 500;
		response.send({status: "BAD", message: error});
	})
});

// Method used to retrieve the history
// Input : session used id
app.get("/history/:username", parser, (request, response) => {
	let query = {
		text: 'SELECT id FROM fredouil.users WHERE identifiant=$1',
		values: [request.params.username]
	}
	pgsql.ExecuteQuery(query).then(results => {
		query = {
			text: 'SELECT * FROM fredouil.historique WHERE id_user=$1',
			values: [results.message.rows[0].id],
		};
		pgsql.ExecuteQuery(query).then(result => {
			if (result.status == "GOOD") {
				response.statusCode = 200;
				response.send({ message: "GOOD", records: result.message.rows });
			} else {
				response.statusCode = 500;
				response.send({ message: "ERROR" });
			}
		});
	});
});

// Method used to get the score sum of all quizz for each user
// Output : for each user : id, avatar, name, sum of all scores
app.get("/ranking", parser, (request, response) => {
	let query = {
		text: 'SELECT identifiant, statut_connexion, avatar, id_user, SUM(score) as total FROM fredouil.users JOIN fredouil.historique ON (fredouil.users.id = fredouil.historique.id_user) GROUP BY fredouil.historique.id_user, identifiant, avatar, statut_connexion',
		values: [],
	}

	pgsql.ExecuteQuery(query).then(result => {
		if(result.status == "GOOD") {
			response.statusCode = 200;
			response.send({ message: "GOOD", records: result.message.rows})
		} else {
			result.statusCode = 500;
			response.send({ message: "ERROR" });
		}
	})
});

// Method used to fetch the players that have the most victories
app.get("/top-ten", parser, (request, response) => {
	let query = {
		text: 'SELECT identifiant, avatar, COUNT(id_user_gagnant) FROM fredouil.hist_defi INNER JOIN fredouil.users ON fredouil.users.id = fredouil.hist_defi.id_user_gagnant GROUP BY fredouil.users.identifiant, fredouil.users.avatar ORDER BY COUNT(id_user_gagnant) DESC LIMIT 10',
		values: []
	};

	pgsql.ExecuteQuery(query).then(results => {
		response.statusCode = 200;
		response.send({message: "GOOD", records: results.message.rows});
	});
});

// Method used to change the profile picture of a user
app.patch("/change-avatar", parser, (request, response) => {
	let query = {
		text: 'UPDATE fredouil.users SET avatar=$1 WHERE id=$2',
		values: [request.body.avatar, request.session.userId]
	};

	pgsql.ExecuteQuery(query).then(result => {
		response.statusCode = 200;
		response.send({message: "GOOD"});
	})
});

// Method used to change the mood of a user
app.patch("/change-mood", parser, (request, response) => {
	let query = {
		text: 'UPDATE fredouil.users SET humeur=$1 WHERE id=$2',
		values: [request.body.mood, request.session.userId]
	};

	pgsql.ExecuteQuery(query).then(result => {
		response.statusCode = 200;
		response.send({message: "GOOD"});
	})
});

// Method used to fetch in the MongoDB the challenges that a user is target of
app.get("/pending-challenges", parser, (request, response) => {
	MONGO_DB.collection("defis").find({receiver: request.session.username}).toArray((error, receiverRecords) => {
		if (error != null) {
			response.statusCode = 500;
			response.send({ message: "Unable to execute query" });
			return;
		}
		MONGO_DB.collection("defis").find({sender: request.session.username}).toArray((error, senderRecords) => {
			if (error != null) {
				response.statusCode = 500;
				response.send({ message: "Unable to execute query" });
				return;
			}
			response.statusCode = 200;
			response.send({asReceiver: receiverRecords, asSender: senderRecords});
		});
	});
});

// Method used to cancel a pending challenge and remove it from the database
app.post("/deny-challenge", parser, (request, response) => {
	MONGO_DB.collection("defis").deleteOne({_id: new mongodb.ObjectID(request.body.challengeId)}, (error, result) => {
		if(error != null || result.result.n != 1) {
			response.statusCode = 500;
			response.send({ message: "Unable to execute query" });
			return;
		}
		response.statusCode = 200;
		response.send({message: "GOOD"});
	});
});

var server = app.listen(PORT, () => {
	console.log("Server starting...");
	MongoClient.connect(DB_URL, {useUnifiedTopology: true}, (error, client) => {
		if (error != null) {
			console.log("Unable to connect to MongoDB database");
			exit(-1);
		}
		MONGO_DB = client.db("db");
	});
});

// Web sockets
const io = require('socket.io')(server, {
	cors: {
		origin: CLIENT_URL,
		methods: ["GET", "POST"]
	}
});

io.on('connection', client => {
	// Used to store in the MongoDB data concerning a challenge between two players
	client.on('challenge', data => {
		let challenges = MONGO_DB.collection("defis");
		let entry = {
			sender: data.sender,
			receiver: data.receiver,
			theme: data.theme,
			difficulty: data.difficulty,
			score: data.score
		};
		challenges.insertOne(entry);
		// We check if the receiver is connected
		let query = {
			text: "SELECT statut_connexion FROM fredouil.users WHERE identifiant=$1",
			values: [data.receiver]
		}
		pgsql.ExecuteQuery(query).then(results => {
			if(results.message.rows[0].statut_connexion == 0) { // If not connected, we add the notification into a pending list
				let entry = {
					sender: data.sender,
					receiver: data.receiver
				};
				MONGO_DB.collection("pending-notifications").insertOne(entry);
			} else { // If connected, we emit the notification
				io.emit('newChallenge', {
					sender: data.sender,
					receiver: data.receiver
				});
			}
		});
	});
});