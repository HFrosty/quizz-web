const { Client } = require('pg');

function CreateClient() {
    return new Client({
        user: 'uapv1903831',
        host: 'pedago01c.univ-avignon.fr',
        database: 'etd',
        password: 'DmYnbC',
        port: 5432
    });
}

module.exports.ExecuteQuery = function (query, parameters) {
    return new Promise((fullfill, reject) => {
        let client = CreateClient();
        client.connect();
        client.query(query, (error, result) => {
            if (error) {
                console.log(error);
                reject({ status: "ERROR", message: error });
            } else {
                fullfill({ status: "GOOD", message: result });
            }
            client.end();
        });
    });
}